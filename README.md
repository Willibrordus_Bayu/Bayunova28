<h1 align="center">Hi 👋, I'm Willibrordus Bayu</h1>

<p align="left"> <img src="https://komarev.com/ghpvc/?username=bayunova28&label=Profile%20views&color=0e75b6&style=flat" alt="bayunova28" /> </p>

- 📚 I’m currently studying on **Multimedia Nusantara University**

- 🖥️ I’m a major of **Information System**

- 📊 I’m currently learning **Data Science**

- ✉️ How to reach me : **willibayu28@gmail.com**

<h3 align="left">Connect with me:</h3>
<p align="left">
<a href="https://www.linkedin.com/in/willibrordusbayu/" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/linked-in-alt.svg" alt="https://www.linkedin.com/in/willibrordusbayu/" height="30" width="40" /></a>
<a href="https://www.instagram.com/bayunova28/" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/instagram.svg" alt="https://www.instagram.com/bayunova28/" height="30" width="40" /></a>
</p>

<h3 align="left">Languages and Tools:</h3>
<p align="left"> <a href="https://www.w3schools.com/css/" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-original-wordmark.svg" alt="css3" width="40" height="40"/> </a> <a href="https://www.w3.org/html/" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original-wordmark.svg" alt="html5" width="40" height="40"/> </a> <a href="https://www.mysql.com/" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/mysql/mysql-original-wordmark.svg" alt="mysql" width="40" height="40"/> </a> <a href="https://www.python.org" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-original.svg" alt="python" width="40" height="40"/> </a> </p>

<p><img align="left" width = "45%" src="https://github-readme-stats.vercel.app/api/top-langs?username=bayunova28&show_icons=true&locale=en&layout=compact" alt="bayunova28" /></p>
<p>&nbsp;<img align="center" width = "50%" src="https://github-readme-stats.vercel.app/api?username=bayunova28&show_icons=true&locale=en" alt="bayunova28" /></p>
